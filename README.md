# OpenML dataset: road_safety

https://www.openml.org/d/43069

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data reported to the police about the circumstances of personal injury road accidents in Great Britain from 1979, and the maker and model information of vehicles involved in the respective accident

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43069) of an [OpenML dataset](https://www.openml.org/d/43069). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43069/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43069/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43069/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

